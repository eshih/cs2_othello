#include "exampleplayer.h"
#include <iostream>


Board *b;
Side s;
Side oppside;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */

ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    b = new Board();
    s = side;
    if (s == BLACK)
       oppside = WHITE;
    else
       oppside = BLACK;

}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    b->doMove(opponentsMove, oppside); //updates board with opponent's move
    
    if (b->isDone())
        return NULL;

    int bestScore = -10000;
    Move *bestMove = NULL;
    Board *temp;

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            temp = b->copy();
            if (b->checkMove(new Move(i, j), s))
            {
                temp->doMove(new Move(i,j), s);
                int checkScore = getBest(b, s, 8);
                if ((i == 0 and j == 0) or (i == 0 and j == 7) or (i == 7 and j == 0) or (i == 7 and j == 7))
                    checkScore += 30;
                if (i == 0 or j == 0 or i == 7 or j == 7)
                        checkScore += 10;

                if (checkScore > bestScore)
                {
                    bestScore = checkScore;
                    bestMove = new Move(i,j);
                }
            }
        }
    }
    b->doMove(bestMove, s);
    return bestMove;
}

int ExamplePlayer::getBest(Board *board, Side side, int num)
{
    Board *temp;
    int bestScore = board->count(side) - board->count(oppside);

    if (num > 0 and !board->isDone())
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                temp = b->copy();
                if (b->checkMove(new Move(i, j), s))
                {
                    temp->doMove(new Move(i,j), s);
                    num = num - 1;
                    int checkScore = getBest(temp, side, num);
                    if ((i == 0 and j == 0) or (i == 0 and j == 7) or (i == 7 and j == 0) or (i == 7 and j == 7))
                        checkScore += 30;
                    if (i == 0 or j == 0 or i == 7 or j == 7)
                        checkScore += 20;
                    if (checkScore > bestScore)
                        bestScore = checkScore;
                }
            }
        }
    }
    return bestScore; 
}