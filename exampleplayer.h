#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

struct node 
{
    Move *m;
    int s;
    node *next;
};

class ExamplePlayer {

    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int getBest(Board *board, Side s, int num);

};



#endif
